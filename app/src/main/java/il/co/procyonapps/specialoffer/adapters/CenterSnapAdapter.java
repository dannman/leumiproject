package il.co.procyonapps.specialoffer.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

/**
 * Created by Hanan on 3/1/2018.
 */

public class CenterSnapAdapter extends LinearSnapHelper {
    private final String TAG = this.getClass().getSimpleName();


    @Override
    public View findSnapView(RecyclerView.LayoutManager layoutManager) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;

        if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
            return linearLayoutManager.findViewByPosition(0);
        } else if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == linearLayoutManager.getItemCount() - 1) {
            return linearLayoutManager.findViewByPosition(linearLayoutManager.getItemCount() - 1);
        } else {
            return super.findSnapView(layoutManager);
        }
    }


    @Override
    public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
        Log.d(TAG, "findTargetSnapPosition: velocityX=" + velocityX);

        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return RecyclerView.NO_POSITION;
        }

        final View currentView = findSnapView(layoutManager);


        if (currentView == null) {
            return RecyclerView.NO_POSITION;
        }

        int currentPosition = layoutManager.getPosition(currentView);
        Log.d(TAG, "findTargetSnapPosition: snap to position=" + currentPosition);

        if (currentPosition == RecyclerView.NO_POSITION) {
            return RecyclerView.NO_POSITION;
        }

        return currentPosition;
    }
}
