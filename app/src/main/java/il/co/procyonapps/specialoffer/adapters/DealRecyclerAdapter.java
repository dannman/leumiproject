package il.co.procyonapps.specialoffer.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.model_objects.DLObject;
import il.co.procyonapps.specialoffer.view.DealViewHolder;

/**
 * Created by Hanan on 2/1/2018.
 */

public class DealRecyclerAdapter extends RecyclerView.Adapter<DealViewHolder> {

    private ArrayList<DLObject> deals;
    private OnRecyclerItemClickListener mClickListener;

    public DealRecyclerAdapter(ArrayList<DLObject> deals) {
        this.deals = deals;

    }


    @Override
    public DealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deal, parent, false);

        return new DealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DealViewHolder holder, int position) {
        holder.setData(deals.get(position));
        holder.itemView.setOnClickListener(v -> {
                    if (mClickListener != null) {
                        mClickListener.onItemClicked(deals.get(holder.getLayoutPosition()));
                    }
                }

        );


    }

    @Override
    public int getItemCount() {
        return deals.size();
    }

    public void setItemTouchListener(OnRecyclerItemClickListener itemTouchListener) {
        mClickListener = itemTouchListener;
    }


    public interface OnRecyclerItemClickListener {

         void onItemClicked(DLObject deal);

    }

}
