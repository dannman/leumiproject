package il.co.procyonapps.specialoffer.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.adapters.MainPagerAdapter;
import il.co.procyonapps.specialoffer.model_objects.Tabs;

/**
 * Created by Hanan on 2/1/2018.
 */

public class MainFrag extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        List<Tabs> tabsList = Arrays.asList(Tabs.values());

        TabLayout tabLayout = root.findViewById(R.id.tab_layout);
        ViewPager viewPager = root.findViewById(R.id.main_view_pager);

        tabLayout.setupWithViewPager(viewPager);

        viewPager.setAdapter(new MainPagerAdapter(getChildFragmentManager(), getContext(), tabsList));
        viewPager.setCurrentItem(tabsList.size() - 1);


        return root;
    }
}
