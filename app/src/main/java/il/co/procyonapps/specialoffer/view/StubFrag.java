package il.co.procyonapps.specialoffer.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import il.co.procyonapps.specialoffer.R;

/**
 * Created by Hanan on 2/1/2018.
 */

public class StubFrag extends Fragment {

    private static final String CONTENT = "CONTENT";

    public static StubFrag newInstance(String text) {

        Bundle args = new Bundle();
        args.putString(CONTENT, text);

        StubFrag fragment = new StubFrag();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stub, container, false);

        ((TextView)root.findViewById(R.id.tv_stub)).setText(getArguments().getString(CONTENT));

        return root;
    }
}
