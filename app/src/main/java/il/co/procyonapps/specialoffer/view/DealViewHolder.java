package il.co.procyonapps.specialoffer.view;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.model_objects.DLObject;

/**
 * Created by Hanan on 2/1/2018.
 */

public class DealViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTitle;
    private final ImageView mImage;
    private final View mItemView;


    public DealViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        mTitle = mItemView.findViewById(R.id.deal_text);
        mImage = mItemView.findViewById(R.id.deal_image);


    }

    public void setData(DLObject deal){
        Picasso.with(mItemView.getContext()).load(deal.im).error(R.drawable.failimage).placeholder(R.drawable.loading).into(mImage);
        Spannable spannable = new SpannableString(String.format("%s %s", deal.tt, deal.sTt));
        spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, deal.tt.length(), 0);
        mTitle.setText(spannable);
    }
}
