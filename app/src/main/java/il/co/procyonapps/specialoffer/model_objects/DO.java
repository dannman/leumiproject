
package il.co.procyonapps.specialoffer.model_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DO {

    @SerializedName("DLObject")
    @Expose
    public List<DLObject> dLObject = null;

    @SerializedName("DLCat")
    @Expose
    public List<DLCat> dLCat = null;



}
