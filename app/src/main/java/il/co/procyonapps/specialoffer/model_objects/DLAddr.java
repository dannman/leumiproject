
package il.co.procyonapps.specialoffer.model_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DLAddr {

    @SerializedName("Addr")
    @Expose
    public String addr;
    @SerializedName("DAd")
    @Expose
    public String dAd;

    @Override
    public String toString() {
        return "DLAddr{" +
                "addr='" + addr + '\'' +
                ", dAd='" + dAd + '\'' +
                '}';
    }
}
