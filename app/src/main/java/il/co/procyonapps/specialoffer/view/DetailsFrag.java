package il.co.procyonapps.specialoffer.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.model_objects.DLObject;
import il.co.procyonapps.specialoffer.view_model.MainViewModel;

/**
 * Created by Hanan on 3/1/2018.
 */

public class DetailsFrag extends DialogFragment implements Observer<DLObject> {

    private static final String DEAL_ID = "DEAL_ID";
    private TextView mCategotyTv;
    private TextView mIdTv;
    private TextView mTitleTv;
    private ImageView mBanner;

    public static DetailsFrag newInstance(int dealId) {

        Bundle args = new Bundle();
        args.putInt(DEAL_ID, dealId);
        DetailsFrag fragment = new DetailsFrag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_deal_details, container, false);
        mCategotyTv = root.findViewById(R.id.tv_category);
        mIdTv = root.findViewById(R.id.tv_id);
        mTitleTv = root.findViewById(R.id.details_title);
        mBanner = root.findViewById(R.id.banner_image);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //get/observe data from viewmodel
        MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getDeal(getArguments().getInt(DEAL_ID)).observe(this, this);
    }

    @Override
    public void onChanged(@Nullable DLObject dlObject) {

        //deal object received - if not null, populate view
        if (dlObject != null) {
            Picasso.with(getContext()).load(dlObject.im).placeholder(R.drawable.loading).error(R.drawable.failimage).into(mBanner);

            Spannable titleSpan = new SpannableString(String.format("%s - %s", dlObject.tt, dlObject.sTt));
            titleSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, dlObject.tt.length() + 1, 0);
            mTitleTv.setText(titleSpan);

            setFirstBoldText(mCategotyTv, String.format(getString(R.string.details_cat), dlObject.catId));
            setFirstBoldText(mIdTv, String.format(getString(R.string.details_id), dlObject.id));
        }
    }

    private void setFirstBoldText(TextView textView, String text) {
        int firstColon = text.indexOf(":");
        Spannable spannable = new SpannableString(text);
        if (firstColon != -1) {
            spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, Math.min(firstColon + 1, text.length()), 0);
        }
        textView.setText(spannable);
    }
}
