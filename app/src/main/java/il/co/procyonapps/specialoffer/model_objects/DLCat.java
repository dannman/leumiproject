
package il.co.procyonapps.specialoffer.model_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DLCat {

    @SerializedName("CatId")
    @Expose
    public int catId;
    @SerializedName("CTitle")
    @Expose
    public String cTitle;

    @Override
    public String toString() {
        return "DLCat{" +
                "catId=" + catId +
                ", cTitle='" + cTitle + '\'' +
                '}';
    }
}
