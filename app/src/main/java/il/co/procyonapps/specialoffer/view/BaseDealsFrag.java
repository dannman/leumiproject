package il.co.procyonapps.specialoffer.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.model_objects.DLCat;
import il.co.procyonapps.specialoffer.model_objects.DLObject;
import il.co.procyonapps.specialoffer.model_objects.DO;
import il.co.procyonapps.specialoffer.view_model.MainViewModel;

/**
 * Created by Hanan on 3/1/2018.
 */

public abstract class BaseDealsFrag extends Fragment {

    private final String TAG = this.getClass().getSimpleName();
    protected LinearLayout mItemHolder;
    protected ArrayList<View> items = new ArrayList<>();

    protected HashMap<Integer, ArrayList<DLObject>> sortedDeals;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);
        mItemHolder = rootView.findViewById(R.id.list_holder);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        viewModel.getRootLiveData().observe(this, root -> {
            Log.d(TAG, "onCreateView: all deals=\n" + root.dO.dLObject);
            populateCategories(root.dO.dLCat);
            sortDealsByCategory(root.dO);
            bindDealsToCategory(items, sortedDeals);
        });
    }

    private void populateCategories(List<DLCat> categories) {

        for (DLCat category : categories) {
            //inflate
            RelativeLayout categoryBase = (RelativeLayout) LayoutInflater.from(getContext()).inflate(getViewRes(), null, false);

            categoryBase.setTag(category.catId);
            items.add(categoryBase);
            TextView title = categoryBase.findViewById(R.id.tv_category_title);
            title.setText(category.cTitle);

            mItemHolder.addView(categoryBase);


        }

    }

    private void sortDealsByCategory(DO baseData){
        sortedDeals = new HashMap<>();
        for(DLObject deal : baseData.dLObject){
            int dealCategory = deal.catId;
            if(!sortedDeals.containsKey(dealCategory)){
                final ArrayList<DLObject> dlObjects = new ArrayList<>();
                dlObjects.add(deal);
                sortedDeals.put(dealCategory, dlObjects);
            } else {
                ArrayList<DLObject> deals = sortedDeals.get(deal.catId);
                deals.add(deal);
            }
        }

        for(ArrayList<DLObject> deals : sortedDeals.values()){
            Log.d(TAG, "sortDealsByCategory: total= " + deals.size() + "\n" + deals.toString());
        }
    }

    protected void showDeal(int id) {
        DetailsFrag.newInstance(id).show(getChildFragmentManager(), null);
    }

    protected abstract void bindDealsToCategory(ArrayList<View> items, HashMap<Integer, ArrayList<DLObject>> sortedDeals);

    @LayoutRes
    abstract int getViewRes();
}
