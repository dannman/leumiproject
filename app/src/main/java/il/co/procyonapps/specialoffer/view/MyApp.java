package il.co.procyonapps.specialoffer.view;

import android.app.Application;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

/**
 * Created by Hanan on 2/1/2018.
 */

public class MyApp extends Application {

    public static WeakReference<MyApp> instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = new WeakReference<MyApp>(this);
        Picasso.with(this).setLoggingEnabled(true);

    }
}
