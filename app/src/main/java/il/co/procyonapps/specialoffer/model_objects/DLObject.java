
package il.co.procyonapps.specialoffer.model_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DLObject {

    @SerializedName("CatId")
    @Expose
    public int catId;
    @SerializedName("Tt")
    @Expose
    public String tt;
    @SerializedName("STt")
    @Expose
    public String sTt;
    @SerializedName("Im")
    @Expose
    public String im;
    @SerializedName("Id")
    @Expose
    public int id;
    @SerializedName("DLAddr")
    @Expose
    public List<DLAddr> dLAddr = null;
    @SerializedName("NOV")
    @Expose
    public String nOV;
    @SerializedName("ClET")
    @Expose
    public boolean clET;

    @Override
    public String toString() {
        return "DLObject{" +
                "catId=" + catId +
                ", tt='" + tt + '\'' +
                ", sTt='" + sTt + '\'' +
                ", im='" + im + '\'' +
                ", id=" + id +
                ", dLAddr=" + dLAddr +
                ", nOV='" + nOV + '\'' +
                ", clET=" + clET +
                '}';
    }
}
