package il.co.procyonapps.specialoffer.custom_views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * Created by Hanan on 3/1/2018.
 */

public class ScrollDetectRecyclerView extends RecyclerView {
    private final String TAG = this.getClass().getSimpleName();

    private final float MILLISECS_PER_INCH = 3.5f;
    private boolean afterScroll;

    public ScrollDetectRecyclerView(Context context) {
        super(context);
    }

    public ScrollDetectRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollDetectRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public void offsetLeftAndRight(int offset) {
        super.offsetLeftAndRight(offset);
        Log.d(TAG, "offsetLeftAndRight: " + offset);
    }

    @Override
    public boolean hasFixedSize() {
        return true;
    }

    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        Log.d(TAG, "onScrollStateChanged: new state=" + state);
        final LinearLayoutManager layoutManager = (LinearLayoutManager) getLayoutManager();
        if (state == SCROLL_STATE_IDLE && afterScroll) {

            afterScroll = false;

            int visiblePos = layoutManager.findFirstCompletelyVisibleItemPosition();
            Log.d(TAG, "onTouchEvent: visiblepos=" + visiblePos);

            snapSmoothScroller.setTargetPosition(visiblePos);
            layoutManager.startSmoothScroll(snapSmoothScroller);
        } else if (state == SCROLL_STATE_DRAGGING) {
            afterScroll = true;
        }
    }


    SmoothScroller snapSmoothScroller = new LinearSmoothScroller(this.getContext()) {
        @Override
        protected int getHorizontalSnapPreference() {
            return LinearSmoothScroller.SNAP_TO_END;
        }

        @Override
        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return MILLISECS_PER_INCH / displayMetrics.density;
        }
    };
}
