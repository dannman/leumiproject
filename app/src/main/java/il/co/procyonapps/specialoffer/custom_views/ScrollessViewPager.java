package il.co.procyonapps.specialoffer.custom_views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Hanan on 2/1/2018.
 */

public class ScrollessViewPager extends ViewPager {
    public ScrollessViewPager(Context context) {
        super(context);
    }

    public ScrollessViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
