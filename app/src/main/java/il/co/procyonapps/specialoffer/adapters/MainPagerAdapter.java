package il.co.procyonapps.specialoffer.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

import il.co.procyonapps.specialoffer.model_objects.Tabs;
import il.co.procyonapps.specialoffer.view.AllDealsFrag;
import il.co.procyonapps.specialoffer.view.FavoritesFrag;
import il.co.procyonapps.specialoffer.view.StubFrag;

/**
 * Created by Hanan on 2/1/2018.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private List<Tabs> mTabList;
    private Context mContext;

    public MainPagerAdapter(FragmentManager fm, Context context, List<Tabs> tabList) {
        super(fm);
        mContext = context;
        mTabList = tabList;

    }

    @Override
    public Fragment getItem(int position) {
        switch (mTabList.get(mTabList.size() - 1 - position)){
            case ALL:

                return new AllDealsFrag();

            case RECOMENDED:
                return new FavoritesFrag();

                default:
                    return StubFrag.newInstance(mContext.getString(mTabList.get(mTabList.size() - 1 - position).getTitleId()));
        }

    }

    @Override
    public int getCount() {
        return mTabList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(mTabList.get(mTabList.size() - 1 - position).getTitleId());
    }
}
