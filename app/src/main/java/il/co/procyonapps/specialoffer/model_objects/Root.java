
package il.co.procyonapps.specialoffer.model_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Root {

    @SerializedName("DO")
    @Expose
    public DO dO;

    @Override
    public String toString() {
        return "Root{" +
                "dO=" + dO +
                '}';
    }
}
