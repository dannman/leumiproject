package il.co.procyonapps.specialoffer.view_model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import il.co.procyonapps.specialoffer.api.MockApi;
import il.co.procyonapps.specialoffer.model_objects.DLObject;
import il.co.procyonapps.specialoffer.model_objects.Root;

/**
 * Created by Hanan on 2/1/2018.
 */

public class MainViewModel extends ViewModel {
    private final String TAG = this.getClass().getSimpleName();
    private MutableLiveData<Root> mRootLiveData;
    private MutableLiveData<DLObject> mSelectedDeal;



    public LiveData<Root> getRootLiveData() {
        if (mRootLiveData == null) {
            mRootLiveData = new MutableLiveData<>();

            //initial load:
            MockApi.getData(new MockApi.ApiCallbacks<Root>() {
                @Override
                public void onCallback(Root response) {
                    mRootLiveData.setValue(response);
                }

                @Override
                public void onFail() {
                    Log.e(TAG, "onFail: couldn't load data");
                }
            });

        }
        return mRootLiveData;
    }

    public LiveData<DLObject> getDeal(int id){
        if(mSelectedDeal == null){
            mSelectedDeal = new MutableLiveData<>();
        }

        getRootLiveData().observeForever(root -> {
            for(DLObject deal : root.dO.dLObject){
                if(deal.id == id){
                    mSelectedDeal.setValue(deal);
                    break;
                }
            }
        });

        return mSelectedDeal;
    }


}
