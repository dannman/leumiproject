package il.co.procyonapps.specialoffer.model_objects;

import android.support.annotation.StringRes;

import il.co.procyonapps.specialoffer.R;

/**
 * Created by Hanan on 2/1/2018.
 */

public enum Tabs {
    ALL(R.string.all),
    RECOMENDED(R.string.recomnded),
    TREATS(R.string.treats),
    FAVORITES(R.string.favorites);

    @StringRes
    int titleId;

    Tabs(int titleId) {
        this.titleId = titleId;
    }

    public int getTitleId() {
        return titleId;
    }
}
