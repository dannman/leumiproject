package il.co.procyonapps.specialoffer.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import il.co.procyonapps.specialoffer.model_objects.Root;
import il.co.procyonapps.specialoffer.view.MyApp;

/**
 * Created by Hanan on 2/1/2018.
 */

public class MockApi {

    public static void getData(ApiCallbacks<Root> callbacks){

        Handler handler = new Handler(Looper.getMainLooper());
        Executor uiExecutor = command -> handler.post(command);

        Executors.newSingleThreadExecutor().submit(() ->{
            try {
                InputStream inputStream = MyApp.instance.get().getAssets().open("jsonObject.json");
                int size = inputStream.available();
                byte[] buffer = new byte[size];
                inputStream.read(buffer);
                inputStream.close();

                String root = new String(buffer, "UTF-8");
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                final Root rootObject = gson.fromJson(root, Root.class);
                Log.d("MockApi", "getDeals: " + rootObject.dO.dLObject);
                uiExecutor.execute(() -> callbacks.onCallback(rootObject));

            } catch (IOException e) {
                e.printStackTrace();
                uiExecutor.execute(() -> callbacks.onFail());
            }
        });
    }


    public interface ApiCallbacks<T>{
        void onCallback(T response);
        void onFail();
    }
}
