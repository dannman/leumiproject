package il.co.procyonapps.specialoffer.view;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import il.co.procyonapps.specialoffer.R;
import il.co.procyonapps.specialoffer.adapters.DealRecyclerAdapter;
import il.co.procyonapps.specialoffer.model_objects.DLObject;

/**
 * Created by Hanan on 2/1/2018.
 */

public class AllDealsFrag extends BaseDealsFrag {


    //snap to first binding
    protected void bindDealsToCategory(ArrayList<View> items, HashMap<Integer, ArrayList<DLObject>> sortedDeals){
        for(View category : items){
            int categoryId = (int) category.getTag();
            ArrayList<DLObject> catDeal = sortedDeals.get(categoryId);
            if (catDeal != null) {
                RecyclerView dealRV = category.findViewById(R.id.rv_deals);
                final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);
                layoutManager.setItemPrefetchEnabled(true);
                layoutManager.setInitialPrefetchItemCount(3);
                dealRV.setLayoutManager(layoutManager);
                final DealRecyclerAdapter adapter = new DealRecyclerAdapter(catDeal);
                dealRV.setAdapter(adapter);
                adapter.setItemTouchListener(deal -> {
                    Toast.makeText(getContext(), "clicked: " + deal.tt, Toast.LENGTH_SHORT).show();
                    showDeal(deal.id);
                });
                dealRV.postDelayed(() -> dealRV.scrollToPosition(0), 1000);


            }
        }

    }

    @Override
    int getViewRes() {
        return R.layout.item_category;
    }
}
